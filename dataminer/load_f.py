def load_f(filename="window_switching.json"):
    infile = open(filename, "r")
    contents = infile.read().splitlines()
    infile.close()
    return contents
