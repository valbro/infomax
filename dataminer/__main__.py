#!/usr/bin/env python3

import json
import sys
from collections import OrderedDict

import dataminer as dm

def main(argv):
    if len(argv) > 0:
        str1 = ''.join(dm.load_f(argv[0]))
    else:
        str1 = ''.join(dm.load_f())
    timelist = json.loads(str1)
    events = {}
    for event in timelist:
        str2 = json.dumps(event["window_activity"], ensure_ascii=False)
        if str2 not in events:
            events[str2] = 1
        else:
            events[str2] += 1
    #print(event["window_activity"]["app_info"])
    sort_ev = OrderedDict(sorted(events.items(), key=lambda t: t[1]))
    for k, v in sort_ev.items():
        if v > 1:
            print(v, k, '\n')

if __name__ == "__main__":
    main(sys.argv[1:])
